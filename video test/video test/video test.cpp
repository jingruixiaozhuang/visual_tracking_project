// author: Bohan Zhuang
// 2012.11 at Dalian University of Technology
// coding with hungry at my dorm


#include <opencv2/opencv.hpp>
#include "tld_utils.h"
#include <iostream>
#include <sstream>
#include "TLD.h"
#include <stdio.h>
#include <Windows.h>
#include <string.h>
#include <fstream>
#include <sstream>
using namespace cv;
using namespace std;


void readConfig(char* configFileName, char* imgFilePath, Rect &box);

void readImageSequenceFiles(char* ImgFilePath,vector <string> &imgNames);

int main(int argc, char * argv[])
{
	bool tl=true;
	char imgFilePath[100];
    char  conf[100];
	strcpy(conf,"./config.txt");
	char tmpDirPath[MAX_PATH+1];
	
	Rect box; // [x y width height] tracking position
    vector <string> imgNames;
    
	readConfig(conf,imgFilePath,box);
	readImageSequenceFiles(imgFilePath,imgNames);
	
	
	TLD tld;
	CompressiveTracker ct;
	Mat frame;
	Mat last_gray;

	sprintf(tmpDirPath, "%s/", imgFilePath); //¾ø¶ÔÂ·¾¶
	imgNames[0].insert(0,tmpDirPath);
	
    frame=cvLoadImage(imgNames[0].c_str(),-1); 
	tld.read();
    cvtColor(frame, last_gray, CV_RGB2GRAY); 
	tld.init(last_gray,box);
	ct.init(last_gray,box);//boxÎªµÚÒ»Ö¡³õÊ¼»¯µÄbox
    tld.tracker.init(last_gray,box);

	char strFrame[20];

    FILE* resultStream;
	resultStream = fopen("TrackingResults.txt", "w");
	fprintf (resultStream,"%i %i %i %i\n",(int)box.x,(int)box.y,(int)box.width,(int)box.height);//Ð´ÈëµÚÒ»Ö¡µÄbox
	Mat current_gray;
    BoundingBox pbox;
    bool status=true;
    int frames = 1;
    int detections = 1;

	for(int i = 1; i < imgNames.size()-1; i ++)
	{
		
		sprintf(tmpDirPath, "%s/", imgFilePath);
        imgNames[i].insert(0,tmpDirPath);
       
		frame=cvLoadImage(imgNames[i].c_str(),-1);		
		//frame = imread(imgNames[i]);// get frame
		cvtColor(frame, current_gray, CV_RGB2GRAY);
		tld.processFrame(current_gray,box,pbox,status,tl);//pboxÎª¸ú×ÙµÄµ½µÄbox
		if (status)
		 { drawBox(frame,pbox);
           detections++;
         }
		
        fprintf (resultStream,"%i %i %i %i\n",(int)pbox.x,(int)pbox.y,(int)pbox.width,(int)pbox.height);
        sprintf(strFrame, "#%d ",i) ;

		putText(frame,strFrame,cvPoint(0,20),2,1,CV_RGB(25,200,25));//ÔÚÍ¼ÏñÖÐÏÔÊ¾ÐòºÅstrFrame
		//Display
		imshow("TLD", frame);
		//swap points and images
        swap(last_gray,current_gray);
        frames++;
        fprintf(resultStream, "%d/%d\n",detections,frames);
        waitKey(1);
       
  }	
	
	fclose(resultStream);

	return 0;
}

void readConfig(char* configFileName, char* imgFilePath, Rect &box)	
{
	int x;
	int y;
	int w;
	int h;

	fstream f;
	char cstring[1000];
	int readS=0;

	f.open(configFileName, fstream::in);

	char param1[200]; strcpy(param1,""); //ÔÚparam1 param2 param3ºóÃæ¼ÓÉÏ¿Õ¸ñ½øÐÐ·Ö¸ô
	char param2[200]; strcpy(param2,"");
	char param3[200]; strcpy(param3,"");

	f.getline(cstring, sizeof(cstring));
	readS=sscanf (cstring, "%s %s %s", param1,param2, param3);

	strcpy(imgFilePath,param3);

	f.getline(cstring, sizeof(cstring)); 
	f.getline(cstring, sizeof(cstring)); 
	f.getline(cstring, sizeof(cstring));


	readS=sscanf (cstring, "%s %s %i %i %i %i", param1,param2, &x, &y, &w, &h);

	box = Rect(x, y, w, h);
	
}

void readImageSequenceFiles(char* imgFilePath,vector <string> &imgNames)
{	
	imgNames.clear();

	char tmpDirSpec[MAX_PATH+1];
	sprintf (tmpDirSpec, "%s/*", imgFilePath);

	WIN32_FIND_DATAA f;
	HANDLE h = FindFirstFileA(tmpDirSpec , &f);
	if(h != INVALID_HANDLE_VALUE)
	{
		FindNextFileA(h, &f);	//read ..
		FindNextFileA(h, &f);	//read .
		do
		{
			imgNames.push_back(f.cFileName);
		} while(FindNextFileA(h, &f));

	}
	FindClose(h);	
}