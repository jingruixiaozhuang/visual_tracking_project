# README #

## Overview
This project incorporates the advantages of the TLD tracker[1] and the compressive tracker[2] to formulate a robust and real-time visual tracking system. It's implemented in C++.

## Dependencies
Opencv 2.3.1

## How to run
The main .cpp file is ./video test/video test/video test.cpp. We provide an example video and the initial bounding box in this project.


## References
[1] @article{kalal2012tracking,
  title={Tracking-learning-detection},
  author={Kalal, Zdenek and Mikolajczyk, Krystian and Matas, Jiri},
  journal={IEEE transactions on pattern analysis and machine intelligence},
  volume={34},
  number={7},
  pages={1409--1422},
  year={2012},
  publisher={IEEE}
}

[2] @inproceedings{zhang2012real,
  title={Real-time compressive tracking},
  author={Zhang, Kaihua and Zhang, Lei and Yang, Ming-Hsuan},
  booktitle={European Conference on Computer Vision},
  pages={864--877},
  year={2012},
  organization={Springer}
}